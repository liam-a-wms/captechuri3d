/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;
import com.google.gson.Gson;

/**
 * Source of configuration constants.
 */
public class RobotMap {

    //Motor ID values.
    public static final int FRONT_LEFT_MOTOR = 1;
    public static final int FRONT_RIGHT_MOTOR = 2;
    public static final int BACK_LEFT_MOTOR = 3;
    public static final int BACK_RIGHT_MOTOR = 4;

    //Joystick ID values.
    public static final int CONTROLLER_ID = 0;
    public static final int CONTROLLER_X_AXIS = 1;
    public static final int CONTROLLER_Y_AXIS = 5;
    public static final int CONTROLLER_LEFT_TRIGGER_AXIS = 2;
    public static final int CONTROLLER_RIGHT_TRIGGER_AXIS = 3;
    public static final int CONTROLLER_LEFT_BUMPER = 5;
    public static final int CONTROLLER_RIGHT_BUMPER = 6;
    public static final int CONTROLLER_Y_BUTTON = 4;

    //Intake ID values.
    public static final int OUTTAKE_ID = 6;
    public static final int INTAKE_ID = 5;

    public static Calibration COLOR_VALUES = null;

    static void loadCalibration() {
        try {
            File f = new File(System.getenv().get("HOME") + "/media/sda1/calibration.json");
            Scanner s = new Scanner(f);
            String str = s.useDelimiter("\\Z").next();
            COLOR_VALUES = new Gson().fromJson(str, Calibration.class);
            s.close();
        } catch (Exception e) {
            System.out.println("Failed to get calibration data. Please check the USB connection or create new calibration data.");
        }
    }

    static void saveCalibration() {
        try {
            File f = new File(System.getenv().get("HOME") + "/media/sda1/calibration.json");
            PrintWriter p = new PrintWriter(f);
            p.append(new Gson().toJson(COLOR_VALUES));
            p.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class Calibration {

        public int[] WHEEL_YELLOW = null;
        public int[] WHEEL_GREEN = null;
        public int[] WHEEL_RED = null;
        public int[] WHEEL_CYAN = null;

    }
}
