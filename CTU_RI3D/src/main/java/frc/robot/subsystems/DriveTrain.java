/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.VictorSPX;
import com.ctre.phoenix.motorcontrol.ControlMode;

/**
 * Class managing the Drive Train.
 */
public class DriveTrain {
    private VictorSPX frontLeft, frontRight, backLeft, backRight;

    /**
     * Basic constructor for creating a DriveTrain object
     * 
     * @param frontLeft The ID of the front-left Talon SPX
     * @param frontRight The ID of the front-right Talon SPX
     * @param backLeft The ID of the back-left Talon SPX
     * @param backRight The ID of the back-right Talon SPX
     */
    public DriveTrain(int frontLeft, int frontRight, int backLeft, int backRight) {
        this.frontLeft = new VictorSPX(frontLeft);
        this.frontRight = new VictorSPX(frontRight);
        this.backLeft = new VictorSPX(backLeft);
        this.backRight = new VictorSPX(backRight);
    }

    public void setLeftSpeed(double speed) {
        frontLeft.set(ControlMode.PercentOutput, speed);
        backLeft.set(ControlMode.PercentOutput, speed);
    }

    public void setRightSpeed(double speed) {
        frontRight.set(ControlMode.PercentOutput, speed);
        backRight.set(ControlMode.PercentOutput, speed);
    }

    public void setSpeed(double left, double right) {
        setLeftSpeed(left);
        setRightSpeed(right);
    }
}
