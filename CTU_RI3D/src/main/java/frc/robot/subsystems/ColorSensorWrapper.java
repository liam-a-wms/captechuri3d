/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.revrobotics.ColorSensorV3;

import edu.wpi.first.wpilibj.I2C.Port;

/**
 * Add your docs here.
 */
public class ColorSensorWrapper {
    
    ColorSensorV3 m_sensor;

    static ColorSensorWrapper instance = null;

    private ColorSensorWrapper() {
        m_sensor = new ColorSensorV3(Port.kOnboard);
    }

    public static ColorSensorWrapper getInstance() {
        if(instance != null) return instance;
        else {
            synchronized(ColorSensorWrapper.class) {
                if(instance == null) instance = new ColorSensorWrapper();
            }
            return instance;
        }
    }

    public int[] getColorValue() {
        int[] c = {0, 0, 0}; //R, G, B
        ColorSensorV3.RawColor r = null;
        r = m_sensor.getRawColor();
        c[0] = r.red;
        c[1] = r.green;
        c[2] = r.blue;
        return c;
    }
}
